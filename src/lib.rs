use sha1::{Sha1, Digest};

#[derive(Debug)]
pub struct BicInCittaStation{
    position: drs_primitives::Coord,
    name: String,
    id: String,
    base_url: String
}

impl drs_primitives::providers::BikeSharingStation for BicInCittaStation {
    fn get_position(&self) -> drs_primitives::Coord {
        self.position
    }
    fn get_name(&self) -> Option<String> {
        Some(String::from(self.name.as_str()))
    }
    fn get_id(&self) -> String {
        String::from(self.id.as_str())
    }
    fn has_bikes(&self) -> Option<bool> {
        let mut body = std::collections::HashMap::new();
        body.insert("IDStazione", &self.id);
        let client = reqwest::Client::new();
        let resp: std::collections::HashMap<String,String> = client.post(&format!("{}/RefreshPopup", &self.base_url))
            .json(&body)
            .send().unwrap()
            .json().unwrap();
        let pieces: Vec<&str> = resp["d"].split("§").collect();
        match &pieces.len() {
            9 => {
                match pieces[6] {
                    "2" => Some(false),
                    "0" => {
                        let mut found = false;
                        for ch in pieces[4].chars() {
                            if ch == '4' {
                                found = true;
                                break;
                            }
                        }
                        Some(found)
                    }
                    _ => None
                }
            },
            _ => None
        }
    }
}

#[derive(Debug)]
pub struct BicInCitta {
    stazioni: Vec<BicInCittaStation>,
    comune: String,
    id_comune: String,
    base_url: String,
}

impl BicInCitta {
    pub fn init(base_url: &str, id_comune: &str, nome_comune: &str) -> Self {
        let mut body = std::collections::HashMap::new();
        body.insert("IDComune", id_comune);
        let client = reqwest::Client::new();
        let resp: std::collections::HashMap<String,Vec<String>> = client.post(&format!("{}/RefreshStations", base_url))
            .json(&body)
            .send().unwrap()
            .json().unwrap();
        
        let mut lista_stazioni: Vec<BicInCittaStation> = Vec::new();
        for stazione in &resp["d"] {
            let pieces: Vec<&str> = stazione.split("§").collect();
            match &pieces.len() {
                7 => lista_stazioni.push(BicInCittaStation{
                    base_url: String::from(base_url),
                    id: String::from(pieces[0]),
                    name: String::from(pieces[3]),
                    position: drs_primitives::Coord{
                        lat: pieces[1].parse::<f64>().unwrap_or(0.0),
                        lon: pieces[2].parse::<f64>().unwrap_or(0.0)
                    }
                }),
                _ => continue
            }
        }
        BicInCitta{
            base_url: String::from(base_url),
            comune: String::from(nome_comune),
            id_comune: String::from(id_comune),
            stazioni: lista_stazioni
        }
    }
}

impl drs_primitives::providers::BikeSharingServiceProvider for BicInCitta {
    fn get_id(&self) -> String {
        let mut hasher = Sha1::new();
        // process input message
        hasher.input(format!("{}{}",&self.get_name(), &self.id_comune).as_bytes());
        let result = hasher.result();
        format!("{:x}", result)
    }
    fn get_name(&self) -> String {
        String::from(format!("Bicincittà - {}", self.comune).as_str())
    }
    fn get_station_list(&self) -> Vec<Box<dyn drs_primitives::providers::BikeSharingStation>> {
        let mut res: Vec<Box<dyn drs_primitives::providers::BikeSharingStation>> = Vec::new();
        for stazione in &self.stazioni {
            res.push(Box::from(BicInCittaStation{
                    base_url: String::from(&stazione.base_url),
                    id: String::from(&stazione.id),
                    name: String::from(&stazione.name),
                    position: stazione.position
                }));
        }
        res
    }
    fn get_nearest_station(&self, from: &drs_primitives::Coord) -> Box<dyn drs_primitives::providers::BikeSharingStation> {
        let mut temp: (f64, BicInCittaStation) = (0.0,BicInCittaStation{base_url: String::from(""),id: String::from(""),name: String::from(""),position: drs_primitives::Coord{lat:0.0,lon:0.0}});
        temp.0 = std::f64::MAX;
        for stazione in &self.stazioni {
            if stazione.position.distance(from) < temp.0 {
                temp.0 = stazione.position.distance(from);
                temp.1 = BicInCittaStation{
                    base_url: String::from(&stazione.base_url),
                    id: String::from(&stazione.id),
                    name: String::from(&stazione.name),
                    position: stazione.position
                };
            }
        }
        Box::from(temp.1)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        use drs_primitives::providers::{BikeSharingStation, BikeSharingServiceProvider};
        let stazione = super::BicInCittaStation{id: String::from("1203"), name: String::from("Imola Rossini"), position: drs_primitives::Coord{lat:0.0, lon:0.0}, base_url: String::from("http://www.mimuovoinbici.it/frmLeStazioni.aspx")};
        println!("{:?}", stazione.has_bikes());
        println!("{:?}", super::BicInCitta::init("http://www.mimuovoinbici.it/frmLeStazioni.aspx", "120", "Cesena").get_name());
        assert_eq!(2 + 2, 4);
    }
}
